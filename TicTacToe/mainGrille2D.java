

public class mainGrille2D {

	public static void main(String[] args) 
	{
		// Cette instance de Grille2D ne servira qu'à appeler la fonction testRegressionGrille() : 
		Grille2D uneGrille = new Grille2D() ; 
		// Execution du test et traitement des exceptions : 
		try
		{
			uneGrille.testRegressionGrille();
		}
		catch(Exception exceptionTest)
		{
			System.out.println(exceptionTest.getMessage());
		}
	}
		

}
