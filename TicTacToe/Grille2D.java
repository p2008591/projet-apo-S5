

/** La classe Grille2D permet d'implémenter une grille 3x3 en deux dimensions. Elle hérite pour cela des attributs et méthodes de Grille */
public class Grille2D extends Grille
{
	/** grille2D est un tableau en deux dimensions représentant la grille 3x3. Ses valeurs sont de type caractère.  */
	private char[][] grille2D ; 
	// Note : Grille2D hérite également de tailleGrille = 3, grilleComplete, nbGrilles2D. 


	// Constructeurs : 
	/** Constructeur par défaut de la classe Grille2D, qui fait appel à celui de la classe mère, 
	 * et intialise le tableau représentatif de la grille.
	 */
	public Grille2D()
	{
		// Appel au constructeur de la classe mère : initialisation des attributs hérités
		super() ; 
		this.nbGrilles2D = 1 ; 
		grille2D = new char[3][3] ; 
		initialiserGrille() ; 

	}
	
	// Méthodes héritées de Grille : 
	/**Procédure permettant d'initialiser la grille, avec des caractères allant de 1 à 9. 
	 */
	public void initialiserGrille()
	{
		int i,j ; 
		int motif = 1 ; 
		for(i=0 ; i<tailleGrille; i++)
		{
			for(j=0; j<tailleGrille ; j++)
			{
				// Character.forDigit() permet de convertir un int en char : 
				grille2D[i][j] = Character.forDigit(motif, 10) ;
				motif++ ; 
			}
		}
	}
	
	/**Procédure permettant d'afficher les cases de la sur la sortie standard. 
	 */
	public void afficherGrille() 
	{
		int i,j ; 
		for(i=0 ; i<tailleGrille; i++)
		{
			System.out.print(" | ") ; 
			for(j=0; j<tailleGrille ; j++)
			{
				System.out.print(' ') ; 
				System.out.print(grille2D[i][j]) ; 
				System.out.print(' ') ; 
			}
			System.out.print(" | \n") ; 
		}
		System.out.print('\n'); 
	}
	
	/**Accesseur permettant d'accéder à l'attribut grille2D 
	 * @return getGrille2D() renvoie un tableau de caractères en deux dimensions, qui représente la grille 2D.
	 */
	public char[][] getGrille2D() 
	{
		return grille2D ; 
	}

	

	/**Procédure permettant de modifier la valeur d'une des cases de la grille.
	* @param indiceL représente l'indice de la ligne de la case que l'on souhaite modifier
	* @param indiceC représente l'indice de la colone de la case que l'on souhaite modifier
	* @param motif représente le caractère à insérer dans la case.
	*/
	public boolean modifierCase(int indiceL, int indiceC, char motif)
	{
		if((indiceL>=0) && (indiceL<3) && (indiceC>=0) && (indiceC<3))
		{
			grille2D[indiceL][indiceC]=motif;
			return true;
		}
		else
		{
			System.out.println("La case que vous souhaitez sélectionner n'est pas dans la grille.");
			return false;
		}
	}

	/**Fonction qui détermine si toutes les cases de la grille sont remplies, c'est-à-dire
	 * si elles contiennent toutes un motif x ou o. 
	 * @return Elle retourne un booléen qui vaut true si la grille est effectivement remplie
	 */
	public boolean verifGrilleRemplie() 
	{
		boolean remplie = true ; 
		int i, j ; 
		// Parcours de la grille 
		for(i=0 ; i<tailleGrille; i++)
		{
			for(j=0; j<tailleGrille ; j++)
			{
				// Si l'on tombe sur un autre caractère que les motifs 'x' ou 'o' : 
				if(grille2D[i][j]!='x' && grille2D[i][j]!='o')
				{
					remplie = false ; 
				}
			}
			
		}
		// Mise à jour de l'attribut grilleComplete
		grilleComplete = remplie ; 
		// Renvoi d'un booléen, qui permet d'encapsuler l'accès à grilleComplete : 
		return remplie ; 
		
	}
	/** Fonction qui parcoure la grille pour déterminer si une ligne a été completée par un même motif. 
	 * @return verifLignesGrille renvoie un booléen qui vaut true si une ligne a été completée.  */ 
	public boolean verifLignesGrille() 
	{
		int i, j ; 
			
		for(i=0 ; i<tailleGrille; i++) // Pour chaque ligne de la grille 
		{   
			char premiereCase = grille2D[i][0] ; 
			int nbOccurences = 1 ; 
			
			// Parcours des cases de la ligne en comptant le nombre d'occurrences identiques 
			for(j=1; j<tailleGrille ; j++)
			{
				if(grille2D[i][j] == premiereCase )
				{
					nbOccurences++ ; 
				}

			}
			// Trois motifs identiques sur une même ligne <=> ligne completée 
			if(nbOccurences == tailleGrille)
			{
				afficherLigneGagnante2D(i);
				return true ; 
			}
			
		}
		return false ; 
	}
	
	/** Fonction qui parcoure la grille pour déterminer si une colonne a été completée par un même motif. 
	 * @return verifColonnesGrille renvoie un booléen qui vaut true si une colonne a été completée.  */ 
	public boolean verifColonnesGrille()
	{
		int i, j ; 
		for(j=0 ; j<tailleGrille; j++) // Pour chaque colonne de la grille 
		{    
			char premiereCase = grille2D[0][j] ; 
			int nbOccurences = 1 ; 
			// Parcours des cases de la colonne en comptant le nombre d'occurrences identiques 
			for(i=1; i<tailleGrille ; i++)
			{
				if(grille2D[i][j] == premiereCase )
				{
					nbOccurences++ ; 
				}
			}
			// Trois motifs identiques sur une même colonne <=> colonne completée 
			if(nbOccurences == tailleGrille)
			{
				// Affichage spécial de la colonne trouvée : 
				afficherColonneGagnante2D(j);
				return true ; 
			}
			
		}
		return false ; 
	}
	
	/** Fonction qui parcoure les diagonales de la grille et déterminent si l'une d'elles a été completée par un même motif. 
	 * @return verifDiagonaleGrille renvoie un booléen qui vaut true si une diagonale a été completée.  */ 
	public boolean verifDiagonalesGrille() 
	{
		// On teste par des boucles, afin que la méthode soit adaptable pour d'autres valeurs de tailleGrille 
		int i, j, k  ; 
		// Test de la première diagonale : cases pour lesquelles i=j 
		boolean diagonale1Remplie = true ;
		char motif = grille2D[0][0] ; 
		for(i=1 ; i<tailleGrille ; i++)
		{
			if(grille2D[i][i] != motif )
			{
				diagonale1Remplie = false ; 
				
			}
		}

		// Test de la deuxième diagonale, dans la direction opposée :
		// On part de la dernière case de la première ligne puis pour chaque case, i augmente de 1 et j diminue de 1 
		boolean diagonale2remplie = true ; 
		motif = grille2D[0][tailleGrille - 1] ; 
		i = 1 ; 
		j = tailleGrille - 2 ; 
		for(k=0 ; k<tailleGrille-1 ; k++)
		{
			if( grille2D[i][j] != motif )
			{
				diagonale2remplie = false ; 
			}
			i++ ; 
			j-- ; 
		}

		if(diagonale1Remplie) afficherDiagonaleGagnante2D(1);
		if(diagonale2remplie) afficherDiagonaleGagnante2D(2);
		return(diagonale1Remplie||diagonale2remplie); 

	}

	/** Fonction qui vérifie, grâce aux fonctions prévues à cet effet, les trois combinaisons :
	 *  lignes, diagonale, colonnes, afin de déterminer si une combinaison a été completée.
	 * @return La fonction renvoie un booléen qui vaut true si une ligne, une colonne, ou la diagonale a été completée.  */ 
	public boolean verifCombinaisons() 
	{
		return ( verifColonnesGrille() || verifLignesGrille() ||  verifDiagonalesGrille() ) ; 
	}

	/**Fonction qui verifie si une case de la grille est vide, c'est-à-dire si elle contient un motif X ou O. 
	 * @param indiceL représente l'indice de la ligne à laquelle on souhaite accéder
	 * @param indiceC représente l'indice de la colone à laquelle on souhaite accéder
	 * @param numG représente le numéro de la grille à laquelle on souhaite accéder 
	 * @return Elle retourne un booléen qui vaut true si la case est vide.
	 */
	public boolean caseVide(int indiceL, int indiceC){
		return (grille2D[indiceL][indiceC]!='x') && (grille2D[indiceL][indiceC]!='o');
	}

	/** Accesseur qui renvoie le caractère se trouvant à la position (indiceL, indiceC) dans la grille
	* @param indiceL représente l'indice de la ligne de la case à laquelle on souhaite accéder
	* @param indiceC représente l'indice de la colone de la case à laquelle on souhaite accéder
	* @return motif représente le caractère à présent dans la case
	*/
	public char getMotif(int indiceL, int indiceC)
	{
		return (grille2D[indiceL][indiceC]);
	}

	/** Mutateur qui modifie le caractère se trouvant à la position (indiceL, indiceC) dans la grille
	* @param indiceL représente l'indice de la ligne à laquelle on souhaite accéder
	* @param indiceC représente l'indice de la colone à laquelle on souhaite accéder
	* @param motif représente le caractère à insérer dans la case.
	*/
	public void setMotif(int indiceL, int indiceC, char motif)
	{
		grille2D[indiceL][indiceC]=motif;
	}

	/**Procédure effectuant l'affichage de la grille, en mettant une ligne gagnante en valeur grâce
	 * à un caractère spécial. 
	* @param indiceL représente l'indice de la colonne gagnante . 
	*/
	public void afficherLigneGagnante2D(int indiceL) 
	{
		int i, j ; 
		System.out.println("Ligne completée : ") ; 
		for(i=0 ; i<tailleGrille; i++)
		{
			if(i==indiceL) System.out.print(" "+" |");
			else System.out.print(" "+" | ");
			for (j=0 ; j<tailleGrille ; j++)
			{
				// Si la ligne courante est la ligne gagnante : 
				if(i==indiceL)
				{
					System.out.print('>') ;
					System.out.print(grille2D[i][j]) ; 
					if(j<tailleGrille-1) System.out.print("< ") ;
					else System.out.print("<") ; 
					
				}
				else
				{
					System.out.print(' ') ;
					System.out.print(grille2D[i][j]) ; 
					System.out.print(' ') ;
				}
			}
			if(i==indiceL) System.out.print("| \n");
			else System.out.print(" | \n");
		}
	}

	/**Procédure effectuant l'affichage de la grille, en mettant une colonne gagnante en valeur grâce
	 * à un caractère spécial. 
	* @param indiceC représente l'indice de la colonne gagnante . 
	*/
	public void afficherColonneGagnante2D(int indiceC) 
	{
		int i,j ; 
		System.out.println("Colonne trouvée : ") ;
		for(i=0 ; i<tailleGrille; i++)
		{
			System.out.print(" | ") ; 
			for(j=0; j<tailleGrille ; j++)
			{
				System.out.print(" ");
				if(j==indiceC) System.out.print('>') ; 
				System.out.print(grille2D[i][j]) ; 
				if(j==indiceC) System.out.print("<"+" ") ; 
			}
			System.out.print(" | \n") ; 
		}
		System.out.print('\n'); 

	}

	/**Procédure effectuant l'affichage de la grille, en mettant une diagonale gagnante en valeur grâce
	 * à un caractère spécial. 
	 * @param indiceD représente l'indice de la diagonale à afficher : 
	 * Si indiceD = 1, alors on affiche la diagonale partant de l'angle gauche supérieur. 
	 * Si indiceD = 2, alors on affiche la diagonale partant de l'angle droit supérieur. 
	*/
	public void afficherDiagonaleGagnante2D(int indiceD) 
	{
		int i,j ; 
		switch(indiceD)
		{
			case 1:
				for(i=0 ; i<tailleGrille; i++)
				{
					System.out.print(" | ") ; 
					for(j=0; j<tailleGrille ; j++)
					{
						System.out.print(" ");
						if(i==j) System.out.print('>') ; 
						System.out.print(grille2D[i][j]) ; 
						if(i==j) System.out.print("<") ; 
						
					}
					System.out.print(" | \n") ; 
				}
				System.out.print('\n'); 
				break ; 

			case 2:
				for(i=0 ; i<tailleGrille; i++)
				{
					System.out.print(" | ") ; 
					for(j=0; j<tailleGrille ; j++)
					{
						boolean diagonale = ((i==0&&j==2)||(i==1&&j==1) ||(i==2&&j==0) ) ; 
						System.out.print(" ");
						if(diagonale) System.out.print('>') ; 
						System.out.print(grille2D[i][j]) ; 
						if(diagonale)System.out.print("<") ; 
						
					}
					System.out.print(" | \n") ; 
				}
				System.out.print('\n'); 
				break ; 
			default:
				System.out.println("Indice de diagonale invalide"); 
				break; 
		}
	}

	/** Procédure effectuant l'affichage de la grille, en mettant en valeur une case choisie par l'utilisateur, dans le but de lui demander par la suite 
	 * s'il confirme ou non son choix
	 * @param indiceL représente la ligne choisie 
	 * @param indiceC représente la colonne choisie 
	 * @param motif représente le motif à afficher dans la case (indiceL, indiceC)
	 */
	public void afficherChoix(int indiceL, int indiceC, char motif )
	{
		int i,j ; 
		for(i=0 ; i<tailleGrille; i++)
		{
			System.out.print(" | ") ; 
			for(j=0; j<tailleGrille ; j++)
			{
				if(i==indiceL && j==indiceC)
				{
					System.out.print('[') ;
					System.out.print(motif) ; 
					System.out.print("]") ; 
				}
				else
				{
					System.out.print(' ') ; 
					System.out.print(grille2D[i][j]) ; 
					System.out.print(' ') ; 
				}
			}
			System.out.print(" | \n") ; 
		}
		System.out.print('\n'); 
	}
	/**Fonction qui effectue une série de tests unitaires afin d'assurer le bon fonctionnement de la classe  Grille2D ou Grille3D.
	 * Elle crée une instance de classe fille uniquement dédiée aux tests, effectue des affichages explicites sur la sortie standard 
	 * et lance des Exceptions qui bloquent l'éxécution en cas d'erreur. Ces exceptions seront traitées 
	 * dans le fichier main associé à la classe.  
	 */
	public void testRegressionGrille() throws Exception 
	{
		// Création d'une instance de Grille2D uniquement dédiée aux tests : 
		// L'initialisation est effectuée dans le constructeur par défaut 
		Grille2D grilleTest = new Grille2D() ;
		// On vérifie que la taille de la grille (qui est une variable de type static int ) est initialisée à 3 : 
		if(grilleTest.getTailleGrille()!=3) throw new Exception("Erreur test unitaire Grille2D - La taille de la grille n'est pas initialisée à 3 ") ; 
		else System.out.println("Taille de la Grille2D initialisée avec succès") ; 

		// On vérifie que la grille contient bien des indices de 1 à 9  et on teste la méthode caseVide : 
		int i,j, k ;
		k = 1 ;  
		int taille = grilleTest.getTailleGrille() ; 
		for(i=1 ; i<taille; i++)
		{
			for(j=0 ; j<taille ; j++)
			{	
				if(grilleTest.getMotif(i,j)!=Character.forDigit(k, 10) )
				{
					k++ ; 
					boolean vide = grilleTest.caseVide(i,j); 
					if(!vide) throw new Exception("Erreur test unitaire Grille2D -> case mal initialisée ") ; 
				} 
				else
				{
					throw new Exception("Erreur test unitaire Grille2D lors de l'initialisation des indices de la grille ") ; 
				} 
			}
		}
		System.out.println("Initialisation des indices de la grille effectuée avec succès") ; 

		// Tests des combinaisons sous forme d'affichages, qui génèrent également des exceptions  : 
		System.out.println(" \n Tests de la vérification des combinaisons dans Grille2D : ") ; 
		// Test de la modification des cases et de la complétion d'une ligne : 
		boolean ligneComplete ;  
		
		System.out.println("Test mutateur -> ajout du 'x' dans la case (0,0) :  ") ; 
		grilleTest.modifierCase(0, 0, 'x');
		grilleTest.afficherGrille();
		ligneComplete = grilleTest.verifLignesGrille() ;
		System.out.println(" booléen ligne complète : "+ligneComplete);

		grilleTest.modifierCase(0, 1, 'x');
		grilleTest.afficherGrille();
		ligneComplete = grilleTest.verifLignesGrille() ;
		System.out.println(" booléen ligne complète : "+ligneComplete);

		grilleTest.modifierCase(0, 2, 'x');
		grilleTest.afficherGrille();
		ligneComplete = grilleTest.verifLignesGrille() ;
		if(ligneComplete) System.out.println(" \n booléen ligne complète : "+ligneComplete);
		else throw new Exception("Erreur test unitaire Grille2D, bug de la fonction de vérification d'une ligne "); 


		// Test de la modification des cases et de la complétion d'une colonne : 
		boolean colonneComplete ;  
		
		grilleTest.modifierCase(0, 0, 'o');
		grilleTest.afficherGrille();
		colonneComplete = grilleTest.verifColonnesGrille() ;
		System.out.println("booléen colonne complète : "+colonneComplete);

		grilleTest.modifierCase(1, 0, 'o');
		grilleTest.afficherGrille();
		colonneComplete = grilleTest.verifColonnesGrille() ;
		System.out.println("booléen colonne complète : "+colonneComplete);

		grilleTest.modifierCase(2, 0, 'o');
		grilleTest.afficherGrille();
		colonneComplete = grilleTest.verifColonnesGrille() ;
		if(colonneComplete) System.out.println(" \n booléen colonne complète : "+colonneComplete); 
		else throw new Exception("Erreur test unitaire Grille2D, bug de la fonction de vérification d'une colonne "); 

		// Test de la modification des cases et de la complétion de l'une des deux diagonales :
		boolean diag1Complete ;  
		
		grilleTest.modifierCase(0, 2, 'x');
		grilleTest.afficherGrille();
		diag1Complete = grilleTest.verifDiagonalesGrille() ;
		System.out.println("booléen diagonale 1 complète : "+diag1Complete);

		grilleTest.modifierCase(1, 1, 'x');
		grilleTest.afficherGrille();
		diag1Complete = grilleTest.verifDiagonalesGrille() ;
		System.out.println("booléen diagonale 1 complète : "+diag1Complete);

		grilleTest.modifierCase(2, 0, 'x');
		grilleTest.afficherGrille();
		diag1Complete = grilleTest.verifDiagonalesGrille() ;
		if(diag1Complete) System.out.println(" \n booléen diagonale 1 complète : "+diag1Complete); 
		else throw new Exception("Erreur test unitaire Grille2D, bug de la fonction de vérification d'une diagonale "); 

		// Test de la modification des cases et de la complétion de l'une des deux diagonales :
		boolean diag2Complete ;  
		
		grilleTest.modifierCase(0, 0, 'o');
		grilleTest.afficherGrille();
		diag2Complete = grilleTest.verifDiagonalesGrille() ;
		System.out.println("Diagonale 2 complète : "+diag2Complete);

		grilleTest.modifierCase(1, 1, 'o');
		grilleTest.afficherGrille();
		diag2Complete = grilleTest.verifDiagonalesGrille() ;
		System.out.println("Diagonale 2 complète : "+diag2Complete);

		grilleTest.modifierCase(2, 2, 'o');
		grilleTest.afficherGrille();
		diag2Complete = grilleTest.verifDiagonalesGrille() ;
		if(diag2Complete) System.out.println(" \n booléen diagonale 2 complète : "+diag2Complete); 
		else throw new Exception("Erreur test unitaire Grille2D, bug de la fonction de vérification d'une diagonale "); 

		// Test de verifGrilleRemplie et de get/setMotif : 
		System.out.println("booléen grille remplie = " + grilleTest.verifGrilleRemplie() ) ;
		if(grilleTest.verifGrilleRemplie()== true ) throw new Exception("Erreur test unitaire Grille2D, bug de la fonction de vérification du remplissage de la grille "); 
		System.out.println("Test accesseur -> motif case (2,2) : " + grilleTest.getMotif(2, 2 )  ) ; 
		grilleTest.setMotif(2,2,'o') ; 
		grilleTest.setMotif(1,2,'o') ; 
		grilleTest.setMotif(2,1,'o') ; 
		// La grille est maintenant remplie : 
		grilleTest.afficherGrille();
		if(grilleTest.verifGrilleRemplie()==false ) throw new Exception("Erreur test unitaire Grille2D, bug de la fonction de vérification du remplissage de la grille "); 
		System.out.println("booléen grille remplie = " + grilleTest.verifGrilleRemplie() ) ;
		System.out.println("\n ****Test de régréssion de Grille2D finalisé avec succès****") ; 
	}

}
