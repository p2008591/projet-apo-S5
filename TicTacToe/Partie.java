import java.util.Scanner;
/**Classe permettant de gérer une partie de TicTacToe en 2D ou en 3D, en s'appuyant sur les classes Joueur et Grille.  */ 
public class Partie 
{
    /** joueur1 correspond au premier joueur de la partie, qui se verra attribuer le motif 'x'.  */
    private Joueur joueur1;
    /** joueur2 correspond au deuxième joueur de la partie, qui se verra attribuer le motif 'o'.  */
    private Joueur joueur2;
    /** mode correspond au mode de la partie, qui donne la dimension de la grille  : si mode=2, il s'agira d'une partie en 2D.
     * Si mode=3, il s'agira d'une partie en 3D
       */
    private int mode ; 
    /** grille correspond à la grille du Tic Tac Toe. Grille2D et Grille3D étant des classes filles de Grille, son initialisation 
     * se fera par upcasting de la classe Grille2D ou Grille3D, en fonction du mode de la partie. 
     */
    private Grille grille;
    
	/** 
     * Constructeur de la classe Partie, qui intialise l'attribut mode selon le choix du joueur grâce à choixModePartie()
     * et affecte à Grille, par upcasting de Grille2D ou Grille3D.
	 */
    public Partie() 
    {		
    	String modeChoisi = choixModePartie() ; 
        mode = Integer.parseInt(modeChoisi)  ; 
    	switch(mode)
    	{
			case 2:
				grille = new Grille2D() ; 
				break ; 
			case 3 : 
				grille = new Grille3D() ; 
				break ; 
			default:
				break ; 
    	}

    }
    
    /** Procédure qui réalise l'initialisation des attributs joueur1 et joueur2 de la partie en récuperant leurs noms en console
     * et en leur attribuant respectivement les motifs 'x' et 'o'
     */
    public void initialiserJoueurs()
    {
        Scanner choixJoueur = new Scanner(System.in);

        System.out.println("Entrez le nom du joueur 1 et appuyez sur le bouton ENTRER");
        String nom1 = choixJoueur.nextLine() ; 
        System.out.println("Bienvenue, "+nom1+" !");

        System.out.println("Entrez le nom du joueur 2 et appuyez sur le bouton ENTRER");
        String nom2 = choixJoueur.nextLine() ; 
        System.out.println("Bienvenue, "+nom2+" !");

        joueur1 = new Joueur('x', nom1);
        joueur2 = new Joueur('o', nom2);

    }
    
    /**Fonction permettant au joueur de choisir le mode du jeu (en 3D ou en 2D).Il contrôle la validité de la saisie utilisateur, afin 
     * d'intialiser correctement l'attribut mode dans le constructeur. 
     * @return choixModePartie() renvoie une chaîne de caractères correspondant au mode saisi par le joueur. 
	 */
    public String choixModePartie()
    {
      Scanner val = new Scanner(System.in);
      String modeChoisi ; 
      do 
      {
    	  System.out.println("Veuillez selectionner un mode de Jeu (2 pour une Grille2D, 3 pour une Grille3D)");
    	  modeChoisi = val.nextLine();
    	  if (!modeChoisi.equalsIgnoreCase("2") && !modeChoisi.equalsIgnoreCase("3") )
    	  {
    		System.out.println("Mode de jeu invalide ! ");
    	  }
      }
      while ( !modeChoisi.equalsIgnoreCase("2") && !modeChoisi.equalsIgnoreCase("3") ) ; 
      System.out.println("Vous avez choisi le mode "+modeChoisi) ; 
      return modeChoisi ; 
    }

    /**
     * Procédure permettant à un joueur de choisir une case de destination, ainsi qu'une grille sélectionnée s'il s'agit d'une partie en 3D, et de placer 
     * un pion dans cette case. 
     * @param joueur correspond au joueur qui souhaite placer le pion dans la grille.
	 */
    public void placerPionGrille(Joueur joueur)
    {
        if(mode==2) // Partie en 2D 
        {
            int indiceCase;
            Scanner pos = new Scanner(System.in);
            System.out.println(joueur.getNom() + ", entrez votre prochaine position et appuyez sur le bouton ENTRER");
            indiceCase = pos.nextInt();
            switch(indiceCase)
            {
                // Rappel : choixCase se charge de vérifier la validité de la case, et de demander confirmation ou non du choix 
                case 1:
                    choixCase2D(joueur, 0, 0); 
                break ; 
                case 2: 
                    choixCase2D(joueur, 0, 1); 
                break ; 
                case 3:
                    choixCase2D(joueur, 0, 2); 
                break ; 
                case 4:
                    choixCase2D(joueur, 1, 0); 
                break ; 
                case 5:
                    choixCase2D(joueur, 1, 1); 
                break ; 
                case 6:
                    choixCase2D(joueur, 1, 2); 
                break ; 
                case 7:
                    choixCase2D(joueur, 2, 0); 
                break ; 
                case 8: 
                    choixCase2D(joueur, 2, 1); 
                break ; 
                case 9:
                    choixCase2D(joueur, 2, 2); 
                break ; 
                default: 
                    erreurMouvement(joueur) ; 
                break ;        
            }
        }
        else if(mode==3) // Partie en 3D 
        {
            // Choix de la grille  : 
            int numGrille;
            Scanner sc1 = new Scanner(System.in);
            do
            {
                //Demande au joueur la grille qu'il veut sélectionner
                System.out.println(joueur.getNom() + ",Donnez le numéro de la grille que vous souhaitez modifier : 0 pour (a), 1 pour (b), 2 pour (c) :");
                numGrille = sc1.nextInt();
                if(numGrille>grille.getNombreGrille()-1 || numGrille < 0 )
                {
                    System.out.println("Indice de grille invalide ! ");
                }
            }
            while(numGrille>grille.getNombreGrille()-1 || numGrille < 0 ); 
            // Choix de la case : 
            int indiceCase;
            Scanner pos = new Scanner(System.in);
            System.out.println(joueur.getNom() + ", entrez votre prochaine position et appuyez sur le bouton ENTRER");
            indiceCase = pos.nextInt();
            switch(indiceCase)
            {
                // Rappel : choixCase2D se charge de vérifier la validité de la case, et de demander confirmation ou non du choix 
                case 1:
                    choixCase3D(joueur, 0, 0, numGrille) ; 
                break ; 
                case 2: 
                    choixCase3D(joueur, 0, 1, numGrille) ; 
                break ; 
                case 3:
                    choixCase3D(joueur, 0, 2, numGrille) ; 
                break ; 
                case 4:
                    choixCase3D(joueur, 1, 0, numGrille) ; 
                break ; 
                case 5:
                    choixCase3D(joueur, 1, 1, numGrille) ; 
                break ; 
                case 6:
                    choixCase3D(joueur, 1, 2, numGrille) ; 
                break ; 
                case 7:
                    choixCase3D(joueur, 2, 0, numGrille) ; 
                break ; 
                case 8: 
                    choixCase3D(joueur, 2, 1, numGrille) ; 
                break ; 
                case 9:
                    choixCase3D(joueur, 2, 2, numGrille) ; 
                break ; 
                default: 
                    erreurMouvement(joueur) ; 
                break ;        
            }
        }
    }
    /** Procédure permettant, dans le cadre d'une partie en 2D, de gérer le choix d'une case par l'utilisateur. Elle 
     * se charge donc de vérifier la validité de la saisie, d'afficher le choix du joueur, et de lui demander confirmation, 
     * en affichant des messages d'erreurs lorsque nécessaire (case occupée, symbole invalide)
     * @param joueur correspond au joueur qui souhaite placer le pion dans la grille.
     * @param indiceL correspond à la ligne de la case de destination du pion dans la grille
     *  @param indiceC correspond à la colonne de la case de destination du pion dans la grille
      */
    public void choixCase2D(Joueur joueur, int indiceL, int indiceC) 
    {
        // Vérification du type de la grille : 
        if(grille instanceof Grille2D)
        {
            // Downcasting pour pouvoir faire appel aux méthodes spécifiques à Grille2D
            Grille2D grille2D = (Grille2D) grille ; 
            // On vérifie d'abord la validité de la case : 
            if(!grille2D.caseVide(indiceL,indiceC))
            {
                System.out.println(" Erreur - mouvement invalide : la case est déjà occupée !");
                placerPionGrille(joueur) ; 
            } 
            System.out.println("Mouvement valide ");
            // Si la case est valide, confirmation du choix : 
            Scanner confirmation = new Scanner(System.in);
            String choixFinal ; 
            boolean confirme ;
            do 
            {
                // Affichage du choix entre crochets : 
                grille2D.afficherChoix(indiceL, indiceC, joueur.getMotif()) ; 
                System.out.println("Veuillez confirmer votre choix (O/N) ");
                choixFinal = confirmation.next();
                if(choixFinal.equalsIgnoreCase("O")) // Si le joueur confirme son choix, 
                { 
                    // On modifie la case souhaitée  : 
                    if(grille2D.caseVide(indiceL,indiceC))
                    {
                        grille2D.modifierCase(indiceL, indiceC, joueur.getMotif()) ; 
                        grille = grille2D ; 
                        // System.out.println("case modif : ") ; grille.afficherGrille() ; 
                    }
                }
                else if (choixFinal.equalsIgnoreCase("N"))
                {
                    placerPionGrille(joueur) ; 
                }
                else 
                {
                    System.out.println("Erreur - Symbole de confirmation invalide ");
                }
            }
            while(!choixFinal.equalsIgnoreCase("O") && !choixFinal.equalsIgnoreCase("N") ) ; 
        }
    }

    /** Procédure permettant, dans le cadre d'une partie en 3D, de gérer le choix d'une case par l'utilisateur. Elle 
     * se charge donc de vérifier la validité de la saisie, d'afficher le choix du joueur, et de lui demander confirmation, 
     * en affichant des messages d'erreurs lorsque nécessaire (case occupée, symbole invalide)
     * @param joueur correspond au joueur qui souhaite placer le pion dans la grille.
     * @param indiceL correspond à la ligne de la case de destination du pion dans la grille
     * @param indiceC correspond à la colonne de la case de destination du pion dans la grille
     * @param numG correspond à l'indice de la grille choisie par le joueur. 
      */
    public void choixCase3D(Joueur joueur, int indiceL, int indiceC, int numG) 
    {
        // Vérification du type de la grille : 
        if(grille instanceof Grille3D)
        {
            // Downcasting pour pouvoir faire appel aux méthodes spécifiques à Grille2D
            Grille3D grille3D = (Grille3D) grille ; 
            // On vérifie d'abord la validité de la case : 
            if(!grille3D.caseVide(indiceL,indiceC, numG))
            {
                System.out.println(" Erreur - mouvement invalide : la case est déjà occupée !");
                placerPionGrille(joueur) ; 
            } 
            System.out.println("Mouvement valide ");
            // Si la case est valide, confirmation du choix : 
            Scanner confirmation = new Scanner(System.in);
            String choixFinal ; 
            boolean confirme ;
            do 
            {
                // Affichage du choix entre crochets : 
                grille3D.afficherChoix(indiceL, indiceC, joueur.getMotif(), numG) ; 
                System.out.println("Veuillez confirmer votre choix (O/N) ");
                choixFinal = confirmation.next();
                if(choixFinal.equalsIgnoreCase("O")) // Si le joueur confirme son choix, 
                { 
                    // On modifie la case souhaitée  : 
                    if(grille3D.caseVide(indiceL,indiceC,numG))
                    {
                        grille3D.modifierCase(indiceL, indiceC, joueur.getMotif(), numG) ; 
                        grille = grille3D ; 
                        
                    }
                }
                else if (choixFinal.equalsIgnoreCase("N"))
                {
                    placerPionGrille(joueur) ; 
                }
                else 
                {
                    System.out.println("Erreur - Symbole de confirmation invalide ");
                }
            }
            while(!choixFinal.equalsIgnoreCase("O") && !choixFinal.equalsIgnoreCase("N") ) ; 
        }
    }
    


    /**Procédure permettant d'envoyer un message d'erreur dans le cas d'un mouvement impossible
     * et de donner au joueur le droit de choisir une autre case, en faisant appel à placerPionGrille. 
     * @param joueur correspond au joueur ayant tenté d'effectuer le mouvement
	 */
    public void erreurMouvement(Joueur joueur)
    {
        System.out.println("Mouvement Impossible !  L'indice de case est invalide.  ");
        placerPionGrille(joueur) ;
    }

    /**Fonction indiquant si la partie est terminée, c'est-à-dire si l'un des deux joueurs a gagné 
     * ou si la grille est complètement remplie. 
     * @param nbToursJ1 correspond au nombre de coups joués par le joueur 1. 
     * @param nbToursJ2 correspond au nombre de coups joués par le joueur 2. 
     * @return finPartie renvoie un booléen qui vaut true si la partie est effectivement terminée. 
	 */
    public boolean finPartie(int nbToursJ1, int nbToursJ2)
    {
        boolean fin = false ; 
        // La partie prend fin si : 
        if(grille.verifCombinaisons()) // si l'un des deux joueurs a gagné : 
        {
            if(nbToursJ1 > nbToursJ2)
            {
                System.out.println("Félicitations, "+joueur1.getNom()+", vous remportez cette partie ! ") ;
            }
            else
            {
                System.out.println("Félicitations, "+joueur2.getNom()+", vous remportez cette partie ! ") ;
            }
            fin = true ; 
 
        }
        else if(grille.verifGrilleRemplie()) // Ou si la grille est complètement remplie : 
        {
            System.out.println("La grille est remplie : partie terminée :( ") ; 
            fin = true ; 
        }
        return fin ; 
    }

    /**Procédure correspondant à la boucle de jeu, factorisée en une seule méthode pour les grilles 2D et 3D. 
     * Elle fait appel aux méthodes de la classe pour initialiser les joueurs, effectuer des affichages de la grille,
     *  solliciter les choix de cases des joueurs, placer leurs pions et afficher un message lorsque la partie est terminée. 
	 */
    public void boucleJeu()
    {
    	initialiserJoueurs() ; 
    	boolean finPartie = false  ; 
    	int nbToursJ1 = 0 ; 
    	int nbToursJ2 = 0 ; 
        boolean fin = false  ; 
    	while(!fin)
    	{

            grille.afficherGrille();
            // On demande au joueur dont c'est le tour de placer un pion et on augmente son nombre de tours : 
            placerPionGrille(joueur1) ; 
            nbToursJ1++ ; 
            fin = finPartie(nbToursJ1, nbToursJ2) ; 
            if(fin) System.exit(-1) ;
            // Une fois le pion placé, on affiche la grille et on passe à l'autre joueur 
            grille.afficherGrille();
            placerPionGrille(joueur2) ; 
            nbToursJ2++ ; 
            fin = finPartie(nbToursJ1, nbToursJ2) ; 
            if(fin) System.exit(-1) ;
    	}
    }
}
