/** La classe Grille3D permet d'implémenter un tableau de grilles en deux dimensions. Elle hérite pour cela des attributs et méthodes de Grille */
public class Grille3D extends Grille
{
	/** @param  grille3D est un tableau de grille2D. */
	private Grille2D[] grille3D ;
	
	// Constructeurs : 
	/** Constructeur par défaut de la classe Grille3D, qui fait appel à celui de la classe mère, 
	 * et intialise le tableau.
	 */
	public Grille3D()
	{
		// Appel au constructeur de la classe mère : initialisation des attributs hérités
		super();
		super.nbGrilles2D=3;
		grille3D = new Grille2D[nbGrilles2D] ; 
		initialiserGrille() ; 
	}
	
	// Méthodes héritées de Grille : 
	/**Procédure permettant d'initialiser les grilles du tableau, avec des caractères allant de 1 à 9 
	 * en faisant appel à la méthode d'initialisation de la classe Grille2D
	 */
	public void initialiserGrille()
	{
		int i;
		for(i=0; i<nbGrilles2D; i++)
			{
				grille3D[i] = new Grille2D() ;
			}
	}
	
	/**Procédure permettant d'afficher les grilles du tableau en faisant appel à la méthode d'affichage de 
	 * la classe Grille2D
	 */
	public void afficherGrille() 
	{
		char tab[]= {'a','b','c'};
		int i;
		for(i=0; i<nbGrilles2D; i++)
			{
				System.out.println("("+tab[i]+")");
				grille3D[i].afficherGrille();
			}
	}
	
	/** Procédure effectuant l'affichage de la grille, en mettant en valeur une case choisie par l'utilisateur, dans le but de lui demander par la suite 
	 * s'il confirme ou non son choix
	 * @param indiceL représente la ligne choisie 
	 * @param indiceC représente la colonne choisie 
	 * @param motif représente le motif à afficher dans la case (indiceL, indiceC)
	 * @param numGrille représente la grille sur laquelle on souhaite effectuer l'affichage 
	 */
	public void afficherChoix(int indiceL, int indiceC, char motif, int numGrille )
	{
		System.out.print("\n"+"(") ; 
		switch(numGrille)
		{
			case 0:
			System.out.print("a)") ;
			break ; 
			case 1:
			System.out.print("b)") ;
			break ; 
			case 2:
			System.out.print("c)") ;
			break ; 
		}
		System.out.print("\n") ; 
		grille3D[numGrille].afficherChoix(indiceL, indiceC, motif) ;
	}


	/**Fonction qui détermine si toutes les grilles du tableau sont remplies, c'est-à-dire
	 * si elles contiennent toutes un motif 'x' ou 'o'. 
	 * @return Elle retourne un booléen qui vaut true si les grilles sont effectivement remplies
	 */
	public boolean verifGrilleRemplie() 
	{
		boolean remplie=false; 

		// Si toutes les grilles sont remplies
		if(grille3D[0].verifGrilleRemplie() && grille3D[1].verifGrilleRemplie() && grille3D[2].verifGrilleRemplie()) 
		{
				remplie = true ; 
		}
		
		return remplie;
	}
	
	/** Fonction qui parcoure les grilles du tableau pour déterminer si une ligne a été completée par un même motif. 
	 * Si c'est le cas, elle met en évidence la ligne completée, grâce à un caractère spécial. 
	 * @return verifLignesGrille() renvoie un booléen qui vaut true si une ligne a été completée.  */ 
	public boolean verifLignesGrille() 
	{
		boolean combi=false; //booléen indiquant s'il y a une combinaison dans une ligne d'une des grilles

		int i;
		for(i=0; i<nbGrilles2D; i++) // Pour chacune des trois Grille2D
		{
			if(grille3D[i].verifLignesGrille()) // Si une grille contient une combinaison dans l'une de ses lignes
			{
				combi = true ; 
			}
		}
		
		return combi;
	}
	
	/** Fonction qui parcoure les grilles du tableau pour déterminer si une colonne a été completée par un même motif. 
	 * Si c'est le cas, elle met en évidence la ligne completée, grâce à un caractère spécial'. 
	 * @return verifColonnesGrille() renvoie un booléen qui vaut true si une colonne a été completée.  */ 
	public boolean verifColonnesGrille() 
	{
		boolean combi=false; //booléen indiquant s'il y a une combinaison dans une colonne d'une des grilles

		int i;
		for(i=0; i<nbGrilles2D; i++) // Pour chacune des trois Grille2D
		{
			if(grille3D[i].verifColonnesGrille()) // Si une grille contient une combinaison dans l'une de ses colonnes
			{
				combi = true ; 
			}
		}
		
		return combi;
	}
	
	/** Fonction qui parcoure les diagonales des grilles du tableau et détermine si elle a été completée par un même motif. 
	 * Si c'est le cas, elle met en évidence la diagonale, grâce à un caractère spécial. 
	 * @return verifDiagonaleGrille() renvoie un booléen qui vaut true si la diagonale a été completée.  */ 
	public boolean verifDiagonalesGrille() 
	{
		boolean combi=false; //booléen indiquant s'il y a une combinaison dans une diagonale d'une des grilles

		int i;
		for(i=0; i<nbGrilles2D; i++) // Pour chacune des trois Grille2D
		{
			if(grille3D[i].verifDiagonalesGrille()) // Si une grille contient une combinaison dans l'une de ses diagonales
			{
				combi = true ; 
			}
		}
		
		return combi;
	}
	
	/** Fonction qui parcoure les grilles du tableau en étant parallèle à un axe et détermine 
	 * s'il y a une combinaison faite par le même motif dans une colonne. 
	 * Si c'est le cas, elle met en évidence la combinaison, grâce à un caractère spécial. 
	 * @return verifAlignParalleleColonne() renvoie un booléen qui vaut true si une telle combinaison est présente.  */ 
	public boolean verifAlignParalleleColonne() 
	{
		char motif1='x';
		char motif2='o';
		boolean combi=false;
		int i,j;
		for(j=0; j<nbGrilles2D; j++)
		{
			for(i=0; i<nbGrilles2D; i++)
			{
				/* Si les trois grilles forment une combinaison en colonne de façon parallèle à un axe
				 */
				//test avec motif1
				if ( (grille3D[0].getMotif(j, i)==motif1) && (grille3D[1].getMotif(j, i)==motif1) && (grille3D[2].getMotif(j, i)==motif1) )
				{
					afficherAlignParalleleGagnantColonne(j,i);
					combi=true;
				}
				//test avec motif2
				else if( (grille3D[0].getMotif(j, i)==motif2) && (grille3D[1].getMotif(j, i)==motif2) && (grille3D[2].getMotif(j, i)==motif2) )
				{
					afficherAlignParalleleGagnantColonne(j,i);
					combi=true;
				}
			}
		}
		return combi;
	}
	
	/**Procédure effectuant l'affichage des grilles, en mettant la combinaison gagnante en valeur grâce
	 * à un caractère spécial. 
	* @param indiceL représente l'indice de la ligne gagnante 
	* @param indiceC représente l'indice de la colonne gagnante  
	*/
	public void afficherAlignParalleleGagnantColonne(int indiceL, int indiceC)
	{
		int k, i, j ; 
		char tab[]= {'a','b','c'};
		System.out.println("Axe completé : ") ; 
		for(k=0 ; k<nbGrilles2D; k++)
		{
			System.out.println("("+tab[k]+")");
			for(i=0; i<tailleGrille; i++) 
			{
				if(i==indiceL) System.out.print(" "+" |");
				else System.out.print(" "+" | ");
				
				for (j=0 ; j<tailleGrille ; j++)
				{
					System.out.print(" ");
					if(j==indiceC && i==indiceL)
					{
						System.out.print('>') ; 
					}
					else
					{
						System.out.print(" ");
					}
					System.out.print(grille3D[k].getMotif(i,j)) ; 
					if(j==indiceC && i==indiceL)
					{
						System.out.print('<') ; 
					}
					else
					{
						System.out.print(" ");
					} 
				}
				if(i==indiceL) System.out.print("| \n");
				else System.out.print(" | \n");
			}
			
		}
	}
	
	/** Fonction qui parcoure les grilles du tableau en étant parallèle à un axe et détermine 
	 * s'il y a une combinaison faite par le même motif dans une ligne. 
	 * Si c'est le cas, elle met en évidence la combinaison, grâce à un caractère spécial. 
	 * @return verifAlignParalleleLigne() renvoie un booléen qui vaut true si une telle combinaison est présente.  */ 
	public boolean verifAlignParalleleLigne()
	{
		char motif1='x';
		char motif2='o';
		boolean combi =false;
		int i;
		for(i=0; i<nbGrilles2D; i++)
		{
			// Si les trois grilles forment une combinaison en ligne de façon parallèle à un axe
			//test avec motif1
			if ( (grille3D[0].getMotif(i, 0)==motif1) && (grille3D[1].getMotif(i, 1)==motif1) && (grille3D[2].getMotif(i, 2)==motif1) )
			{
				afficherAlignParalleleGagnantLigne(i);
				combi=true;
			}
			//test avec motif2
			else if( (grille3D[0].getMotif(i, 0)==motif2) && (grille3D[1].getMotif(i, 1)==motif2) && (grille3D[2].getMotif(i, 2)==motif2) )
			{
				afficherAlignParalleleGagnantLigne(i);
				combi=true;
			}
		}
		return combi;
	}
	
	/**Procédure effectuant l'affichage des grilles, en mettant la combinaison gagnante en valeur grâce
	 * à un caractère spécial. 
	* @param indiceL représente l'indice de la ligne gagnante 
	*/
	public void afficherAlignParalleleGagnantLigne(int indiceL)
	{
		int k, i, j ; 
		char tab[]= {'a','b','c'};
		System.out.println("Axe completé : ") ; 
		for(k=0 ; k<nbGrilles2D; k++)
		{
			System.out.println("("+tab[k]+")");
			for(i=0; i<tailleGrille; i++) 
			{
				if(i==indiceL) System.out.print(" "+" |");
				else System.out.print(" "+" | ");
				
				for (j=0 ; j<tailleGrille ; j++)
				{
					System.out.print(" ");
					if(i==indiceL && j==k)
					{
						System.out.print('>') ; 
					}
					else
					{
						System.out.print(" ");
					}
					System.out.print(grille3D[k].getMotif(i,j)) ; 
					if(i==indiceL && j==k)
					{
						System.out.print('<') ; 
					}
					else
					{
						System.out.print(" ");
					} 
				}
				if(i==indiceL) System.out.print("| \n");
				else System.out.print(" | \n");
			}
		}

	}

	/** Fonction qui parcoure les grilles du tableau de façon diagonale au travers de deux axes et 
	 * détermine s'il y a une combinaison faite par le même motif. 
	 * Si c'est le cas, elle met en évidence la combinaison, grâce à un caractère spécial. 
	 * @return verifAlignDiag2Axes() renvoie un booléen qui vaut true si une telle combinaison est présente.  */ 
	public boolean verifAlignDiag2Axes() 
	{
		char motif1='x';
		char motif2='o';
		boolean combi=false;
		int i;
		for(i=0; i<nbGrilles2D; i++)
		{
			/* Si les trois grilles forment une combinaison de façon diagonale au travers de deux axes
			 */
			//test avec motif1
			if ( (grille3D[0].getMotif(0, i)==motif1) && (grille3D[1].getMotif(1, i)==motif1) && (grille3D[2].getMotif(2, i)==motif1) )
			{
				afficherAlignDiag2AxesGagnant(i);
				combi=true;
			}
			//test avec motif2
			else if( (grille3D[0].getMotif(0, i)==motif2) && (grille3D[1].getMotif(1, i)==motif2) && (grille3D[2].getMotif(2, i)==motif2) )
			{
				afficherAlignDiag2AxesGagnant(i);
				combi=true;
			}

		}
		return combi;
	}
	
	/**Procédure effectuant l'affichage des grilles, en mettant la combinaison gagnante en valeur grâce
	 * à un caractère spécial. 
	* @param indiceC représente l'indice de la colonne gagnante  
	*/
	public void afficherAlignDiag2AxesGagnant(int indiceC)
	{
		char tab[]= {'a','b','c'};
		int k, i, j ; 
		System.out.println("Axe completé : ") ; 
		for(k=0 ; k<nbGrilles2D; k++)
		{
			System.out.println("("+tab[k]+")");
			for(i=0; i<tailleGrille; i++) 
			{
				if(i==indiceC) System.out.print(" | ");
				else System.out.print(" | ");
				
				for (j=0 ; j<tailleGrille ; j++)
				{
					System.out.print(" ");
					if(j==indiceC && i==k) 
					{
						System.out.print('>') ; 
					}
					else
					{
						System.out.print(" ");
					}
					System.out.print(grille3D[k].getMotif(i,j)) ; 
					if(j==indiceC && i==k) 
					{
						System.out.print('<') ; 
					}
					else
					{
						System.out.print(" ");
					}
				}
				if(i==indiceC) System.out.print(" | \n");
				else System.out.print(" | \n");
			}
			
		}
	}
	
	/** Fonction qui parcoure les grilles du tableau de façon diagonale au travers de trois axes et 
	 * détermine s'il y a une combinaison faite par le même motif. 
	 * Si c'est le cas, elle met en évidence la combinaison, grâce à un caractère spécial. 
	 * @return verifAlignDiag3Axes() renvoie un booléen qui vaut true si une telle combinaison est présente. 
	 */ 
	
	public boolean verifAlignDiag3Axes() 
	{
		char motif1='x';
		char motif2='o';
		boolean combi=false;
		//test de la première diagonale

		//test avec motif1
		if ( (grille3D[0].getMotif(0, 0)==motif1) && (grille3D[1].getMotif(1, 1)==motif1) && (grille3D[2].getMotif(2, 2)==motif1) )
		{
			afficherAlignDiag3AxesGagnant();
			combi=true;
		}
		//test avec motif2
		else if( ( (grille3D[0].getMotif(0, 0)==motif2) && (grille3D[1].getMotif(1, 1)==motif2) && (grille3D[2].getMotif(2, 2)==motif2) ) )
		{
			afficherAlignDiag3AxesGagnant();
			combi=true;
		}
		
		
		//test de la seconde diagonale
		//test avec motif1
		if ( (grille3D[0].getMotif(2, 0)==motif1) && (grille3D[1].getMotif(1, 1)==motif1) && (grille3D[2].getMotif(0, 2)==motif1) )
		{
			afficherAlignDiag3AxesGagnant();
			combi=true;
		}
		//test avec motif2
		else if( (grille3D[0].getMotif(2, 0)==motif2) && (grille3D[1].getMotif(1, 1)==motif2) && (grille3D[2].getMotif(0, 2)==motif2) )
		{
			afficherAlignDiag3AxesGagnant();
			combi=true;
		}
	
		return combi;
	}
	
	/**Procédure effectuant l'affichage des grilles, en mettant la combinaison gagnante en valeur grâce
	 * à un caractère spécial. 
	*/
	public void afficherAlignDiag3AxesGagnant()
	{
		char tab[]= {'a','b','c'};
		int k, i, j ; 
		System.out.println("Axe completé : ") ; 
		for(k=0 ; k<nbGrilles2D; k++)
		{
			System.out.println("("+tab[k]+")");
			for(i=0; i<tailleGrille; i++) 
			{
				if(i==k) System.out.print(" | ");
				else System.out.print(" | ");
				
				for (j=0 ; j<tailleGrille ; j++)
				{
					System.out.print(" ");
					if(k==j && j==i) 
					{
						System.out.print('>') ; 
					}
					else
					{
						System.out.print(" ");
					}
					System.out.print(grille3D[k].getMotif(i,j)) ; 
					if(k==j && j==i) 
					{
						System.out.print('<') ; 
					}
					else
					{
						System.out.print(" ");
					}
				}
				if(i==k) System.out.print(" | \n");
				else System.out.print(" | \n");
			}
			
		}
	}
	
	/** Fonction qui vérifie, grâce aux fonctions prévues à cet effet, les six combinaisons :
	 *  lignes, diagonale, colonnes, parallèle à un axe, diagonale au travers de 2 axes, 
	 *  diagonale au travers des 3 axes, afin de déterminer si une combinaison a été completée.
	 * @return La fonction renvoie un booléen qui vaut true si on a une combinaison.  */ 
	public boolean verifCombinaisons() 
	{
		return ( verifColonnesGrille() || verifLignesGrille() ||  verifDiagonalesGrille() || verifAlignParalleleLigne() || verifAlignParalleleColonne()  || verifAlignDiag2Axes()  ||  verifAlignDiag3Axes() ) ;
	}

	/**Procédure permettant de modifier la valeur d'une des cases de la grille. 
	 * @param indiceL représente l'indice de la ligne de la case que l'on souhaite modifier
	 * @param indiceC représente l'indice de la colone de la case que l'on souhaite modifier
	 * @param motif représente le caractère à insérer dans la case
	 * @param numGrille représente le numéro de la grille sélectionnée
	 * @return modifierCase renvoie un booléen qui vaut true si le motif a bien été insérée dans la grille
	 * à la position demandée. 
	 */
	public boolean modifierCase(int indiceL, int indiceC, char motif, int numGrille)
	{
		
		if(grille3D[numGrille].caseVide(indiceL, indiceC)==true)
		{
			grille3D[numGrille].setMotif(indiceL, indiceC, motif);
			return true;
		}
		
		return false;
		
		//grille2D[indiceL][indiceC]=motif;
		//this.setMotif(indiceL, indiceC, motif);
	}
	
	/** Accesseur qui renvoie le caractère se trouvant à la position indice dans la grille
	 * @param indiceL représente l'indice de la ligne à laquelle on souhaite accéder
	 * @param indiceC représente l'indice de la colone à laquelle on souhaite accéder
	 * @param numGrille représente le numéro de la grille sélectionnée
	 * @return getMotif renvoie le caractère présent dans la case.
	 */
	public char getMotif(int indiceL, int indiceC, int numGrille)
	{
		return grille3D[numGrille].getMotif(indiceL, indiceC);
	}
	
	/** Mutateur qui modifie le caractère se trouvant à la position (indiceL, indiceC) dans la grille
	 * @param indiceL représente l'indice de la ligne à laquelle on souhaite accéder
	 * @param indiceC représente l'indice de la colone à laquelle on souhaite accéder
	 * @param numGrille représente le num�ro de la grille sélectionnée
	 * @param motif représente le caractère à insérer dans la case.
	 */
	public void setMotif(int indiceL, int indiceC, char motif, int numGrille)
	{

		if(grille3D[numGrille].caseVide(indiceL, indiceC)==true)
		{
			grille3D[numGrille].setMotif(indiceL, indiceC, motif);
		}
		
	}
	
	/**Fonction qui verifie si une case de la grille est vide, c'est-à-dire si elle contient un motif X ou O. 
	 * @param indiceL représente l'indice de la ligne à laquelle on souhaite accéder
	 * @param indiceC représente l'indice de la colone à laquelle on souhaite accéder
	 * @param numGrille représente le numéro de la grille à laquelle on souhaite accéder
	 * @return Elle retourne un booléen qui vaut true si la case est vide.
	 */
	public boolean caseVide(int indiceL, int indiceC, int numGrille)
	{
		return grille3D[numGrille].caseVide(indiceL,indiceC);
	}

	/**Fonction qui effectue une série de tests unitaires afin d'assurer le bon fonctionnement de la classe Grille3D.
	 * Elle crée une instance de classe fille uniquement dédiée aux tests, effectue des affichages explicites sur la sortie standard 
	 * et lance des Exceptions qui bloquent l'éxécution en cas d'erreur. Ces exceptions seront traitées 
	 * dans le fichier main associé à la classe.  
	 */
	public void testRegressionGrille() throws Exception 
	{
		// Cr�ation d'une instance de Grille3D uniquement d�di�e aux tests : 
		// L'initialisation est effectu�e dans le constructeur par d�faut 
		Grille3D grilleTest = new Grille3D() ;
		// On vérifie que la taille de la grille (qui est une variable de type static int ) est initialisée à 3 : 
		if(grilleTest.getTailleGrille()!=3) throw new Exception("Erreur test unitaire Grille3D - La taille de la grille n'est pas initialisée à 3 ") ; 
		else System.out.println("Taille de la Grille3D initialisée avec succès") ;
		// On vérifie que la taille de la grille (qui est une variable de type static int ) est initialisée à 3 : 
		if(grilleTest.getNombreGrille()!=3) throw new Exception("Erreur test unitaire Grille3D - Le nombre de grilles n'est pas initialisée à 3 ") ; 
		else System.out.println("Nombre de grilles de la Grille3D initialisée avec succès") ;
		
		// Test de l'affichage des grilles
		System.out.println("Test de l'affichage des grilles");
		grilleTest.afficherGrille();
		System.out.println();
	
		//Test de la fonction verifAlignParalleleLigne()
		System.out.println("Test de la fonction verifAlignParalleleLigne()");
		System.out.println("Avant les modifications : "+grilleTest.verifAlignParalleleLigne()); //renvoie false
		grilleTest.modifierCase(0,0,'x',0);
		grilleTest.modifierCase(0,1,'x',1);
		grilleTest.modifierCase(0,2,'x',2);
		grilleTest.afficherGrille();
		System.out.println("Après les modifications : "+grilleTest.verifAlignParalleleLigne()); //renvoie true 
		grilleTest.initialiserGrille();
		System.out.println();
		
		//Test de la fonction verifAlignParalleleColonne()
		System.out.println("Test de la fonction verifAlignParalleleColonne()");
		System.out.println("Avant les modifications : "+grilleTest.verifAlignParalleleColonne()); //renvoie false
		//Sélectionner 3 fois une grille différente pour que verifAlignParalleleColonne() renvoie true
		grilleTest.modifierCase(0,0,'x',0);
		grilleTest.modifierCase(0,0,'x',1);
		grilleTest.modifierCase(0,0,'x',2);
		grilleTest.afficherGrille();
		System.out.println("Après les modifications : "+grilleTest.verifAlignParalleleColonne()); //renvoie true 
		grilleTest.initialiserGrille();
		System.out.println();

		//Test de la fonction verifAlignDiag2Axes()
		System.out.println("Test de la fonction verifAlignDiag2Axes()");
		System.out.println("Avant les modifications : "+grilleTest.verifAlignDiag2Axes()); //renvoie false
		//Sélectionner 3 fois une grille différente pour que verifAlignDiag2Axes() renvoie true
		grilleTest.modifierCase(0,1,'o',0);
		grilleTest.modifierCase(1,1,'o',1);
		grilleTest.modifierCase(2,1,'o',2);
		grilleTest.afficherGrille();
		System.out.println("Après les modifications : "+grilleTest.verifAlignDiag2Axes()); //renvoie true 
		grilleTest.initialiserGrille();
		System.out.println();

		//Test de la fonction verifAlignDiag3Axes()
		System.out.println("Test de la fonction verifAlignDiag3Axes()");
		System.out.println("Avant les modifications : "+grilleTest.verifAlignDiag3Axes()); //renvoie false
		//Sélectionner 3 fois une grille différente pour que verifAlignDiag3Axes() renvoie true
		grilleTest.modifierCase(0,0,'o',0);
		grilleTest.modifierCase(1,1,'o',1);
		grilleTest.modifierCase(2,2,'o',2);
		grilleTest.afficherGrille();
		System.out.println("Après les modifications : "+grilleTest.verifAlignDiag3Axes()); //renvoie true
		grilleTest.initialiserGrille();
		System.out.println();

		System.out.println("\n ****Test de régréssion de Grille3D finalisé avec succès****") ; 
	}

}
