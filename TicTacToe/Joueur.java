/** La classe Joueur permet de regrouper et de modifier les caractéristiques d'un joueur d'une partie de TicTacToe, à savoir 
 * son nom et son motif 'x' ou 'o'.
*/
public class Joueur 
{
	/** motif est un caractère représentant le motif du joueur.  */
    private char motif;
    /** nom est une chaîne de caractères représentant le nom du joueur.  */
    private String nom;
    
    /** Constructeur avec paramètres de la classe Joueur, permettant d'initialiser le motif et le nom
	 * @param motifJ est un caractère qui représente le motif du joueur pour la partie
	 * @param nomJ est une chaîne de caractères qui représente le nom du joueur pour la partie
	 */
    public Joueur(char motifJ, String nomJ)
    {
        motif = motifJ;
        nom = nomJ;
    }

    /** Accesseur du motif du joueur 
	 * @return getMotif() renvoie un caractère correspondant au motif du joueur. 
	 */
    public char getMotif()
    {
        return motif;
    }

    /** Accesseur du nom du joueur 
	 * @return getNom() renvoie une chaîne de caractères correspondant au nom du joueur. 
	 */
    public String getNom()
    {
        return nom;
    }

	/** Mutateur qui modifie le motif du joueur
	 * @param motifJ est un caractère qui représente le nouveau motif du joueur.
	 */
    public void setMotif(char motifJ)
    {
        motif = motifJ;
    }

    /** Mutateur qui modifie le nom du joueur
	 * @param nomJ est une chaîne de caractères qui représente le nouveau motif du joueur.
	 */
    public void setNom(String nomJ)
    {
        nom = nomJ;
    }

}
