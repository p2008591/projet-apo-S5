/**Classe abstraite permettant de factoriser les élements communs aux grilles de jeu tic tac toe, 
 en versions 2D et 3D.  */ 
public abstract class Grille
{
	 /** tailleGrille représente la taille d'une grille. Il est intialisé par défaut à 3. */
	protected static final int tailleGrille = 3 ; 
	/**  grilleComplete est un booléen valant "true" si la grille est entièrement remplie.  */
	protected boolean grilleComplete ; 
	/** nbGrilles2D représente le nombre de grilles qui figurera dans un tableau de grilles,
	 * au sein des classes Grille2D et Grille3D. Il dépendra donc du mode de jeu. */
	protected int nbGrilles2D ;
	
	/** Constructeur par défaut de la classe Grille, qui initialise le booléen grilleComplete
	 * à false, et définit par défaut le nombre de grilles à 1. 
	 */
	public Grille()
	{
		grilleComplete = false ; 
		nbGrilles2D = 1 ; // Le mode de jeu par défaut est 2D
	}
	
	/** Constructeur par paramètres, permettant d'initialiser le nombre de grilles 2D souhaité
	 * @param  nbGrilles2D représente le nombre de grilles choisi 
	 */
	public Grille(int nbGrilles2D)
	{
		grilleComplete = false ; 
		this.nbGrilles2D = nbGrilles2D ; 
	}
	
	/** Accesseur de la taille de la grille 
	 * @return getTailleGrille() renvoie un entier correspondant à la taille de la grille. 
	 */
	public int getTailleGrille()
	{
		return tailleGrille ; 
	}
	
	/** Accesseur du nombre de grilles
	 * @return getNombreGrille() renvoie un entier correspondant au nombre de grilles. 
	 */
	public int getNombreGrille()
	{
		return nbGrilles2D ; 
	}
	
	/* Méthodes abstraites ; elles seront implementées dans Grille2D et Grille3D
	 (D'abord héritées par Grille2D, puis dans Grille3D, on fera, par exemple Grille2D.afficher()
	 Pour chaque grille du tableau) */
	
	/**Procédure permettant d'initialiser la grille, avec des caractères allant de 1 à 9. 
	 */
	public abstract void initialiserGrille(); 
	
	/**Procédure permettant d'afficher les cases de la sur la sortie standard. 
	 */
	public abstract void afficherGrille() ; 
	
	/**Fonction qui détermine si toutes les cases de la grille sont remplies, c'est-à-dire
	 * si elles contiennent toutes un motif X ou O. 
	 * @return Elle retourne un booléen qui vaut true si la grille est effectivement remplie
	 */
	public abstract boolean verifGrilleRemplie() ; // + Met à jour l'attribut grilleComplete 
	
	/** Fonction qui parcoure la grille pour déterminer si une ligne a été completée par un même motif. 
	 * Si c'est le cas, elle met en évidence la ligne completée, grâce à un caractère spécial. 
	 * @return verifLignesGrille renvoie un booléen qui vaut true si une ligne a été completée.  */ 
	public abstract boolean verifLignesGrille() ; 
	
	/** Fonction qui parcoure la grille pour déterminer si une colonne a été completée par un même motif. 
	 * Si c'est le cas, elle met en évidence la ligne completée, grâce à un caractère spécial'. 
	 * @return verifColonnesGrille renvoie un booléen qui vaut true si une colonne a été completée.  */ 
	public abstract boolean verifColonnesGrille() ; 
	
	/** Fonction qui parcoure les diagonales de la grille et détermine si l'une d'elles a été completée par un même motif. 
	 * Si c'est le cas, elle met en évidence la diagonale, grâce à un caractère spécial. 
	 * @return verifDiagonalesGrille renvoie un booléen qui vaut true si la diagonale a été completée.  */ 
	public abstract boolean verifDiagonalesGrille() ; 
	
	/** Fonction qui vérifie, grâce aux fonctions prévues à cet effet, les trois combinaisons :
	 *  lignes, diagonale, colonnes, afin de déterminer si une combinaison a été completée.
	 * @return La fonction renvoie un booléen qui vaut true si une ligne, une colonne, ou la diagonale a été completée.  */ 
	public abstract boolean verifCombinaisons() ; 

	/**Fonction qui effectue une série de tests unitaires afin d'assurer le bon fonctionnement de la classe  Grille2D ou Grille3D.
	 * Elle crée une instance de classe fille uniquement dédiée aux tests, effectue des affichages explicites sur la sortie standard 
	 * et lance des Exceptions qui bloquent l'éxécution en cas d'erreur. Ces exceptions seront traitées 
	 * dans le fichier main associé à la classe et permettront de localiser l'erreur. 
	 */
	public abstract void testRegressionGrille() throws Exception ; 

}
